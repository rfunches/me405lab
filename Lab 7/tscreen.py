''' 
@file tscreen.py

@details This class serves as the hardware driver for the touch screen on the
ball balancing table for the ME 405 term project. Using 4 pin connections to the 
touch screen, the hardware is able to read ball position in the x and y direction, in addition
to determining if the ball is contacting the screen. A settling period is additionally
implemented whenver a reading is taken to ensure accurate results.
'''

import pyb
import utime
from pyb import Pin
from pyb import ADC

class TouchScreen:
    '''
    @brief  This Class implements a TouchScreen'''
    
    def __init__(self,PIN_xm,PIN_xp,PIN_ym,PIN_yp,length,width,center):
        '''
        @brief  Makes TouchScreen object
        @param PIN_xm Pin object correlating to X-
        @param PIN_xp Pin object correlating to X+
        @param PIN_ym Pin object correlating to Y-
        @param PIN_yp Pin object correlating to Y+
        @param length Length of the touchscreen along the x-axis
        @param width  Width of the touchscreen along the y-axis
        @param center  List that holds the x and y coordinates of the center of the table
        '''
        print('Creating a touchscreen driver')
        
        ##Pin object correlating to X-
        self.PIN_xm=PIN_xm
        
        ##Pin object correlating to X+
        self.PIN_ym=PIN_ym
        
        ##Pin object correlating to Y-
        self.PIN_xp=PIN_xp
        
        ##Pin object correlating to Y+
        self.PIN_yp=PIN_yp
        
        ##Length of the touchscreen along the x-axis
        self.length=length
        
        ##Width of the touchscreen along the y-axis
        self.width=width
        
        ##List that holds the x and y coordinates of the center of the table
        self.center=center
        
    def scanX(self):
        '''
        @brief This function scans for the x-position of the ball.
        
        @details This function scans for the x-position of the ball by setting
        the x+ pin high, x- pin low, and reads from y- via analog to digital converter.
        The voltage that y- reads is directly proportional to the x position of the ball, 
        with 0 correlating to x- side of the touchpad and 4095 correlating to the x+ side of the board.

        '''

        self.PIN_xm.init(mode=Pin.OUT_PP,value=0) #set x- low
        self.PIN_xp.init(mode=Pin.OUT_PP,value=1) #set x+ high
        self.PIN_yp.init(mode=Pin.IN) #change mode of y+ to input (so not random) #POSSIBLY SET TO ANALOG
        self.Pin_ym.init(mode=Pin.IN)
        self.PIN_ym.init(mode=Pin.ANALOG) # set y- to analog
        ADC_ym=ADC(self.PIN_ym) #setup ADC on y-
        
        start=utime.ticks_us()
        while(utime.ticks_us()<start+4): #wait 4 microseconds for settling time
            pass
        
        return self.convertX(ADC_ym.read()) #convert reading to distance and return    
    
    def scanY(self):
        '''
        @brief This function scans for the y-position of the ball.
        
        @details This function scans for the y-position of the ball by setting
        the y+ pin high, y- pin low, and reads from x- via analog to digital converter.
        The voltage that x- reads is directly proportional to the y position of the ball, 
        with 0 correlating to y- side of the touchpad and 4095 correlating to the y+ side of the board.

        '''

        self.PIN_ym.init(mode=Pin.OUT_PP,value=0) #set y- low
        self.PIN_yp.init(mode=Pin.OUT_PP,value=1) #set y+ high 
        self.PIN_xp.init(mode=Pin.IN) # set x+ to input (so not random)
        self.Pin_xm.init(mode=Pin.IN)
        self.PIN_xm.init(mode=Pin.ANALOG) #set y- to analog
        ADC_xm=ADC(self.PIN_xm) #setup ADC on xm
        
        start=utime.ticks_us()
        while(utime.ticks_us()<start+4): #wait 4 microseconds for settling time
            pass
        return self.convertY(ADC_xm.read()) #convert reading to distance and return
        
    def scanZ(self):
        '''
        @brief This function scans for if the ball is on the platform
        
        @details This function scans for the existence of the ball on the platform by setting
        the y+ pin high, x- pin low, and reads from y- via analog to digital converter.
        The y- pin reads high when no ball is applied, as it is connected directly 
        to the y+ pin, but ball contact causes a short between y+ and x-, which causes y- to read low.
        True is returned if there is contact with the board, and false is returned otherwise.

        '''
        self.PIN_xm.init(mode=Pin.OUT_PP,value=0) #set x- low
        self.PIN_yp.init(mode=Pin.OUT_PP,value=1) #set y+ high
        self.PIN_xp.init(mode=Pin.IN) #set x+ to input so it is not floating
        self.PIN_ym.init(mode=Pin.ANALOG) #set y- to analog mode
        ADC_ym=ADC(self.PIN_ym) #setup ADC on y-
        
        start=utime.ticks_us()
        while(utime.ticks_us()<start+4): #wait 4 microseconds for settling time
            pass
        print(str(ADC_ym.read())) #print reading for visualization/debugging
        
        if(ADC_ym.read()>=3500): #if short between yp and ym
            return False #no contact with screen
            
        else:
            return True #contact with the screen
        
        
        
        
    def convertX(self,read):
        '''
        @brief This function converts digital voltage readout to x position.
        @param read ADC measurement of voltage between 0 and 4095
        @return dist Distance the ball is in the x-direction from the center of the touchpad in inches
        '''
        PercentAcross=read/4095 #percent across table given by normalizing reading
        DistRightToLeft=PercentAcross*self.length #distance from side to position found by multiplying by length
        dist=DistRightToLeft-center[0] #distance from center found by subtracting center x-coordinate position
        
        return dist
        
    def convertY(self,read):
        '''
        @brief This function converts digital voltage readout to y position.
        @param read ADC measurement of voltage between 0 and 4095
        @return dist Distance the ball is in the y-direction from the center of the touchpad in inches
        '''
        PercentAcross=read/4095 #percent across table given by normalizing reading
        DistRightToLeft=PercentAcross*self.width #distance from side to position found by multiplying by length
        dist=DistRightToLeft-center[1] #distance from center found by subtracting center y-coordinate position
        
        return dist        
    
    def readAll(self):
        '''
        @brief This function scans the x, y, and z directions and returns values
        @return values Tuple containing x, y, and z readings
        @details This function scans the x, y, and z directions and returns a tuple
        of the form (x,y,z). Both x and y are floating point numbers representing 
        position, and z is a boolean that is True when the ball is in contact with the
        board. 
        '''
        start=utime.ticks_us() #start time (for testing)
        x=self.scanX() #scan the x-direction
        y=self.scanY() #scan the y-direction
        z=self.scanZ() #scan the z-direction
        
        values=(x,y,z) #store values in tuple
        print('Refreshing this took '+str(utime.ticks_us()-start)+' microseconds') #display how long it took for the code to run
        return values
        
        
        
        

    

if __name__=='__main__':
    PIN_xm=Pin(Pin.cpu.A6)
    PIN_xp=Pin(Pin.cpu.A0)
    PIN_ym=Pin(Pin.cpu.A1)
    PIN_yp=Pin(Pin.cpu.A7)
    length=7 #inches
    width=4.5 #inches
    center=[length/2,width/2]

    myTouch=TouchScreen(PIN_xm,PIN_xp,PIN_ym,PIN_yp,length,width,center)
    print('Touchpad created')
    
    myTouch.scanX()
    


#4 microseconds settling time
#method for x, y,z convert method
#define ADC's as local to method, as well as all init's
#cooperatively, must run quick
