'''
@file VendoTronFSM.py
@details This file is used as a finite state machine to represent the functionality of 
a vending machine. Users can input coins, drink requests, and eject requests which are immediately processed by the
machine. After an input is processed, the system outputs based on the request and then resets the input. Additionally, a main
method is included, which initializes the keyboard.on_press() method to add input to the system whenever a key is pressed.

@author Ryan Funchess
@date 1/22/21

 
'''

from random import choice
import time
import keyboard

class vendotron:
    '''
    @brief      A finite state machine to control an vending machine.
    @details    This class implements a finite state machine to control the
                operation of a vending machine.
    '''
    
    ##@brief Constant defining State 0
    S0_INIT             = 0    
    
    ##@brief Constant defining State 1
    S1_CoinInsertCheck  = 1    
    
    ##@brief Constant defining State 2
    S2_DrinkSelectCheck = 2    
    
    ##@brief Constant defining State 3
    S3_EjectRequestCheck      = 3    
    
    ##@brief Constant defining State 4
    S4_AddToBalance     = 4
    
   ##@brief Constant defining State 5
    S5_CheckForFunds=5
    
    ##@brief Constant defining State 6
    S6_DistributeDrink=6
    
    ##@brief Constant defining State 7
    S7_EjectCoins=7
    
    ##@details List holding value of input money as a function of the number
    # input by the user for coin input. The user input corresponds to the index
    # of the money value of the input.
    coinval=[1,5,10,25,100,500,1000,2000]
    
    ##@brief List holding drink prices as a function of drink request
    drinkprice=[100,120,85,110]
    
    ##@brief List holding drink names as a function of drink request
    drinknames=['Cuke','Popsi','Spryte','Dr. Pupper']
    
    def __init__(self):
        '''
        @brief      Constructor for a vending machine object.
        @details    This method implements the initial balance, drink request,
        monetary input, and eject request. Initial state is 0.
        '''
        ##@brief Integer holding the current user balance in cents
        self.Balance=0
        
        ##@brief Integer holding the user drink request
        #@details Integer that signifies the drink that the user requests. A value
        # of 10 means that there is no request, 0 means Cuke is requested, 1 means Popsi is
        # requested, 2 means Spryte is requested, and 3 means Dr. Pupper is requested.
        self.DrinkReq=10
        
        ##@brief Integer holding the user monetary input.
        #@details Integer holding user monetary input that has not yet been added
        # to the balance. A money request of 50 means that there is no input, 0 corresponds
        # to 1 cent, 1 to 5 cents, 2 to 10 cents, 3 to 25 cents, 4 to 100 cents, 5 to 500 cents, 
        #6 to 1000 cents, 7 to 2000 cents.
        self.MoneyReq=50
        
        ##@brief Integer holding user eject request
        #@details Integer holding either a 0 or 1, where 0 means no eject request
        # and 1 means the user has input an eject request.
        self.EjectReq=0
        
        ##@brief Integer signifying current state of the vending machine in the FSM.
        self.state=0
        
        
        
    def addInput(self,intake):
        '''
        @brief      Callback function that adds input to system after a key is pressed.
        @details    This method is used in conjunction with keyboard.on_press() such that whenever
        a user types a key, this function is called with intake holding the keystroke. The input is then assigned to 
        a request accordingly.
        @param intake Keyboardevent object that is given when the user presses a key on the keyboard.
        '''
        key=intake.name #take name from keyboardevent object
        if(key=='c'or key=='C'): #if user inputs c, drink requested=0
            self.DrinkReq=0
        elif(key=='p' or key=='P'): #if user inputs p, drink requested=1
            self.DrinkReq=1
        elif(key=='s' or key=='S'): #if user inputs s, drink requested=2
            self.DrinkReq=2
        elif(key=='d' or key=='D'): #if user inputs d, drink requested=3
            self.DrinkReq=3       
        elif(key=='0' or key=='1' or key=='2' or key=='3' or key=='4' or key=='5' or key=='6' or key=='7'): #if input is an int between 0 and 7, money requested
            self.MoneyReq=int(key)
        elif(key=='e' or key=='E'): #if user inputs e, set eject request high
            self.EjectReq=1
        else: #error handling should user input not fit into a request type
            print('Invalid button press')
       
    def printWelcome(self):
        '''
        @brief     Prints a welcome message to the user, including display of prices.
        '''
        print('Welcome New User')
        print('Prices are as follows:')
        print('Cuke: 100 Cents')
        print('Popsi: 120 Cents')
        print('Spryte: 85 cents')
        print('Dr. Pupper: 110 Cents')
        
    def setBalance(self,newBalance):
        '''
        @brief  This method acts as a setter for Balance.
        @param newBalance Integer that Balance is to be set equal to.
        '''
        self.Balance=newBalance
        
    def getChange(self,price):
        '''
        @brief This function returns change to return after a purchase.
        @details This function takes in the price of an item and returns the change
        to be deliverd based on the current balance.
        @param price Integer representing the price of an item
        @return change Integer representing the change to be delivered
        '''
            change=self.Balance-price #change is calculated by sub
            return change
        
    
    def run(self):
        
        '''
        @brief      Runs one iteration of the task
        '''
        
        if(self.state == self.S0_INIT): 
            # Run State 0 Code
             self.printWelcome() #welcome user
             self.transitionTo(self.S1_CoinInsertCheck) #transition to state 1
             
        elif(self.state == self.S1_CoinInsertCheck):
            # Run State 1 Code
            if self.MoneyReq<10: #if MoneyReq is not cleared
                self.transitionTo(self.S4_AddToBalance) #add money to balance
            else:
                self.transitionTo(self.S2_DrinkSelectCheck) #else check for a drink selection
            
        elif(self.state == self.S2_DrinkSelectCheck):
         # Run State 2 Code
         if(not(self.DrinkReq==10)): #if the drink selection is not cleared
             self.transitionTo(self.S5_CheckForFunds) #check for funds
         else:
             self.transitionTo(self.S3_EjectRequestCheck) #else check for eject request
             
         
        elif(self.state == self.S3_EjectRequestCheck):
            #Run state 3 code
            if self.EjectReq==1: #if eject request is set high
                self.transitionTo(self.S7_EjectCoins) #eject the coins
            else:
                self.transitionTo(self.S1_CoinInsertCheck) #else check for inserted coins
         
         
        elif(self.state == self.S4_AddToBalance):
            #Run state 4 code
            self.setBalance(self.Balance+self.coinval[self.MoneyReq]) #Add the input coin value to the balance
            print('New Balance: '+str(self.Balance)) #display new balance
            self.MoneyReq=50 #reset MoneyReq
            self.transitionTo(self.S2_DrinkSelectCheck) #check for drink selection
            
        elif(self.state == self.S5_CheckForFunds):
            #Run state 5 code
            if(self.Balance>=self.drinkprice[self.DrinkReq]): #If the balance is greater than or equal to the drink price
                self.transitionTo(self.S6_DistributeDrink) #distribute the drink
            else: #balance not high enough
                print('Insufficient Funds')
                print('Balance: '+str(self.Balance)) #display balance
                self.DrinkReq=10 #clear drink selection
                self.transitionTo(self.S3_EjectRequestCheck) #check for eject request
            
        elif(self.state == self.S6_DistributeDrink):
            #Run state 6 code
            change=self.getChange(self.drinkprice[self.DrinkReq]) #find the change based on the price of the requested drink
            print('Your change is '+str(change)) #display change (and distributed change if this were a machine)
            self.setBalance(0) #reset balance
            print('Your Balance is now zero')
            print('Here is your yummy drink: '+str(self.drinknames[self.DrinkReq])) #tell them what they ordered
            self.DrinkReq=10 #Reset drink request
            self.printWelcome() #give initialization statement
            self.transitionTo(self.S3_EjectRequestCheck) #transition to eject request
            
        elif(self.state == self.S7_EjectCoins):
            #Run state 7 code
            self.EjectReq=0 #set eject request low
            self.setBalance(0) #reset balance
            print('Your balance has been cleared, and coins delivered back, balance is now zero.') #coins returned
            self.transitionTo(self.S1_CoinInsertCheck) #transition to check for inserted coins
            self.printWelcome() #display initialization statement
            
        else: #error handling
            pass
       
    
    def transitionTo(self,newState):
        '''
        @brief      Updates the variable defining the next state to run
        @param newState Integer representing the next state to be transitioned to
        '''
        self.state = newState
        


if __name__=='__main__':
    myVend=vendotron() #declare new vendotron object
    
    #Below line modeled after this thread: #https://github.com/boppreh/keyboard/issues/152
    keyboard.on_press(myVend.addInput) #Initialization of callback function, on keyboard press call myvend.addInput)
    
    while True: #run FSM forever
        myVend.run()







#keyboard.on_press(addInput)
#import keyboard
#keyboard.on_press
#include print statments
#clear all variables after use (eject, drink request, coin input, balance after drink eject)
#https://github.com/boppreh/keyboard/issues/152












