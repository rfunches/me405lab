'''
@file mainLab3.py

@details This main method communicates with the front end via the serial port,
being allowed to start data collection when the user inputs "G" or "g". Hardware is initialized
for measurment of Analog to Digital conversion to be measured on Pin C13 connected
to the button. Once the button connected to Pin C13 is pressed, the analog voltage values
measured at Pin C13 are converted to a digital format and stored in a buffer array before being
sent back to the front end via the serial port.

'''
import pyb
import utime
from array import array
from pyb import UART

    
def collectData(x):
    '''
    @brief Callback function that sets stall to 0, which starts data collection.
    '''
    global stall #sets stall as global variable so that it can set a variable outside the method
    stall=0    #set stall to 0 to break out of the while loop                      
        
        

if __name__=='__main__': #main method start
    myuart=pyb.UART(2) #initialize UART for communication with computer
    
    #waiting for user input
    n=0 #loop control variable
    while(n==0): #runs while n==0, changed from 0 when user input received
        if myuart.any() != 0: #if there's anything from the computer
            val = myuart.readline().decode('ascii') #read the line
            n=1 #set n to 1 to break the loop
    
    pinC13=pyb.Pin(pyb.Pin.cpu.C13) #initialize PinC13 for the button press
    ButtonInt=pyb.ExtInt(pinC13,mode=pyb.ExtInt.IRQ_RISING,pull=pyb.Pin.PULL_NONE,callback=collectData) #Set up external interrupt,IRQ_RISING because we care when button pushed 
    
    #data array
    buf=array('i',800*[0]) #create buffer array 800 values long of signed integers
    
    #initialize ADC
    adc=pyb.ADC(pyb.Pin.board.PA4) #set pin PA4 as the pin for analog to digital conversion
    tim=pyb.Timer(6,freq=200000) #timer initialized, frequency of 200000
    
    stall=1 #loop control variable
    
    while(stall==1): #run until interrupted
        pass
    
    adc.read_timed(buf,tim) #method that reads adc values to buffer based on timer freq, values are between 0 and 4095
    myuart.write('{:}\r\n'.format(len(buf))) #passes length of list back to front end in order to control loop
    for n in range(len(buf)): #loops through entirety of time and voltage lists
        myuart.write('{:},{:}\r\n'.format(n/200000,buf[n])) #write values of time and voltage seperated by commas at current index
    
