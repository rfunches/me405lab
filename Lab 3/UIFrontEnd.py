'''
@file UIFrontEnd.py

@details This file is used to communicate with the Nucleo via serial communication,
with user input telling the Nucleo when to begin collecting data. Once the Nucleo finishes
collecting data, this file stores time and voltage values passed through the serial port
in lists on the PC. Voltage is then converted back from a digital signal to an analog measurment, plotted
for data visualization, and exported as a csv file for data manipulation.

'''
import array
import time
import serial
import numpy
import matplotlib

ser = serial.Serial(port='COM5',baudrate=115273,timeout=1) #initializes serial port for communication with Nucelo %changed from COM3 to COM5

#prompt user for input
print('Welcome to the UI for Data Collection') #welcomes to frontend

go='' #initializes variable to ensure that the user requests g for data collection
while(not(go=='g' or go=='G')): #loops until the user has entered g or G
    go=input('Press g or G when you are ready for data collection   ') #asks for user input 
    if(not(go=='g' or go=='G')): #if the user inputs something other than g or G, prompt for another response
        print('You need to enter g or G for data collection to begin')
        
start=1 #value to be sent to the Nucleo
ser.write(str(start).encode('ascii')) #writes start to the Nucleo via seria port

time.sleep(10) #wait 10 seconds for the Nucleo to send back data


#Return of Experimental Values
t=[] #initialize time list
voltage=[] #initialize voltage list


vlength=ser.readline().decode('ascii') #obtain length of voltage list
vlength=int(vlength) #voltage list length cast as int, used for loop control

for n in range(1,vlength): #loop through entire list
    line_string = ser.readline().decode('ascii') #read the line which has time and voltage comma seperated
    line_string=(line_string.strip()) #strips all special characters from String, such as \r and \n
    #print(line_string)								# print the line to see in command prompt
    line_list = line_string.split(',')		#split string at comma, 0th entry is time, 1st entry is voltage
    t.append(float(line_list[0]))				#append 0th entry to t list	
    voltage.append(float(line_list[1]))		#append 1st entry to voltage list
    

voltage=[x*3.3/4095 for x in voltage] #convert voltage from digital signal to analog (3.3V in 4095 digital counts)


#Plotting
matplotlib.pyplot.plot(t,voltage,label='Measured')	#plot experimental
matplotlib.pyplot.xlabel('Time (s)') #x-axis label
matplotlib.pyplot.ylabel('Voltage[V]') #y-axis label
matplotlib.pyplot.title('PinC13 Voltage vs Time') #plot title

#save as CSV file
numpy.savetxt('dataoutput.csv',list(zip(t,voltage)),delimiter=',') #output csv file from 2D list
