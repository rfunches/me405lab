'''
@file main.py

@brief This is the main method for Lab 2.
@details This main method initializes all variables relating to 
Lab 2 and runs myLED.run() on an infinite loop until the user interferes.
'''

import pyb
import utime
from random import random
from LEDRun import LED

delay=3 #nominal delay (will add on random )
myLED=LED(delay) #declare LED object

pinC13=pyb.Pin(pyb.Pin.cpu.C13) #initialize PinC13 for the button press
ButtonInt=pyb.ExtInt(pinC13,mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE,callback=myLED.buttonPushed) #Set up external interrupt 

    
while True: #run LED task 
    myLED.run()



#80 MHz clock speed
#Need resolution of 1 microsecond, so user prescaler of 79 