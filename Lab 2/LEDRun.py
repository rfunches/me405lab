'''
@file LEDRun.py

@details This file contains the LED class. After initialization, the run method is run
until users interfere. The method buttonPushed() is a callback function that is run
when the button connected to PinC13 is pushed.
'''
#pyb works with the pins on the board
import pyb

#utime is used to work with millisecond counter on board
import utime
from random import random

class LED:
    '''
    @brief      A finite state machine to control the LED output of the Nucleo.
    @details    This class allows for the operation of a reaction tester, such that 
    when a person pushes a button as soon as they see the glow of a LED, they are delivered their reaction time.
    '''
   
    def __init__(self,delay):
        '''
        @brief            Creates a LED object.
        @param delay   Integer representing the approximate delay between LED flashes. A random addition is made later,
        
        '''
        #Initializes the pin that will be interacted with to use the LED on the board
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        self.tim2 = pyb.Timer(2, freq = 20000)
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        self.t2ch1.pulse_width_percent(0) #sets LED initially to off
        
        ##@brief Boolean where True means button has been pushed, false means that is has not.
        self.Button=False
        
        ##@brief Integer representing the approximate delay between LED flashes. A random addition is made later.
        self.delay=delay
        
    def run(self):
        '''
        @brief This method turns on the LED and delivers the user their performance review.
        @details This function prepares the user to react, turns on the LED at a random time,
        and then displays the reaction time for the user to view. If the user does not react, they are 
        told that they didn't react. Regardless of user choice, the LED is turned off at close of method.

        '''
        print('Get ready to test your reaction') #prompt user
        realdelay=self.delay+random()*3 #take initial delay and add random decimal (<3) to start time
        utime.sleep(realdelay) #stop for realdelay amount of time to keep user in suspense
        self.Button=False #button turned false to prepare for user input
        tim=pyb.Timer(5,prescaler=79,period=0xFFFFFFF) #timer initialized, prescaled so count is in microseconds
        start=tim.counter() #start time recoreded
        #start=utime.ticks_us()
        self.t2ch1.pulse_width_percent(100) #button turned on
        while(not(self.Button)): #while the button is not pressed
            if(tim.counter()-start>10000000): #if the user has not pressed the button after 10 seconds
                print('You did not even react') #prompt them that they did not react
                break #break out of the loop
        if(self.Button): #if the user did press the button
            end=tim.counter() #record the end time
            self.treact=end-start #calculate reaction time
            print('Your reaction time was '+str(self.treact)+' microseconds') #print reaction time
        
        self.t2ch1.pulse_width_percent(0) #turn off LED
        
        
    def buttonPushed(self,x):
        '''
        @brief This method sets Button to True when the physical button is pressed
        @param x Unused variable passed during button press to this method
        '''
        self.Button=True
