def getChange(price,payment):
    #The getChange function takes in an integer price in cents and an integer tuple for payment. The tuple
    # is converted to an integer number of pennies and the difference will yield the change to be returned.
    
    #find the total equivalent number of pennies by converting each
    #denomination to pennies and adding 
    TotalPay=payment[0]+5*payment[1]+10*payment[2]+25*payment[3]+100*payment[4]+500*payment[5]+1000*payment[6]+2000*payment[7]
    
    TotalChange=TotalPay-price #find change by subtracting pay from price
    
    if (TotalChange<0):
        #insufficient funds, return nothing
        return None
    else:
        #Find how many of largest demonination goes in evenly, then go to next smallest, etc
        #could have alternatively done a for loop with a list of values that I wanted to divide, cycle
        #through store the num variables in a list and converted to tuple later
        
        #find how many twenties evenly go into total change, then subtract that amount accordingly
        numTwenty=int(TotalChange/2000)
        TotalChange=TotalChange-2000*numTwenty
        
        #find how many tens evenly go into total change, then subtract that amount accordingly
        numTen=int(TotalChange/1000)
        TotalChange=TotalChange-1000*numTen
        
        #find how many fives evenly go into total change, then subtract that amount accordingly
        numFive=int(TotalChange/500)
        TotalChange=TotalChange-500*numFive
        
        #find how many ones evenly go into total change, then subtract that amount accordingly
        numOne=int(TotalChange/100)
        TotalChange=TotalChange-100*numOne
        
        #find how many quarters evenly go into total change, then subtract that amount accordingly
        numQuart=int(TotalChange/25)
        TotalChange=TotalChange-25*numQuart
        
        #find how many dimes evenly go into total change, then subtract that amount accordingly
        numDime=int(TotalChange/10)
        TotalChange=TotalChange-10*numDime
        
        #find how many nickels evenly go into total change, then subtract that amount accordingly
        numNick=int(TotalChange/5)
        TotalChange=TotalChange-5*numFive
        
        #all that remains are pennies
        numPen=TotalChange
        
        #store all values in change tuple and return
        change=(numPen,numNick,numDime,numQuart,numOne,numFive,numTen,numTwenty)
        return change
        
        
    